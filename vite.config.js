import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      '/api': {
        target: 'https://api.mobygames.com/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      },
      '/Localapi': {
        target: 'http://api:3307',
        //target: 'http://localhost:3307',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/Localapi/, '')
      }
    }
  }
})
