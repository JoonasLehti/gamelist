import axios from "axios";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GameData } from "./DataStore";

const AddNewGame = () => {
  const [title, setTitle] = useState("");
  const [BrotherScore, setBrotherScore] = useState(0);
  const [BigManScore, setBigManScore] = useState(0);
  const [consoleUsed, setConsoleUsed] = useState("");
  const [BrotherStoredComment, setBrotherComment] = useState("");
  const [BigManStoredComment, setBigManComment] = useState("");
  const [info, setInfo] = useState("");
  const [moby_id, setMoby_id] = useState("");
  const [platforms, setPlatform] = useState("");
  const [release_date, setRelease_date] = useState("");
  const [developers, setDevelopers] = useState("");
  const [publishers, setPublishers] = useState("");
  const [moby_score, setMoby_score] = useState("");
  const [genre, setGenre] = useState("");
  const [perspective, setPerspective] = useState("");
  const [gameplay, setGameplay] = useState("");
  const [interfacedata, setInterface] = useState("");
  const [narrative, setNarrative] = useState("");
  const [misc, setMisc] = useState("");
  const [description, setDescription] = useState("");
  const [moby_url, setMoby_url] = useState("");
  const [screenshots_urls, setScreenshots_urls] = useState("");

  const { setStoredGameList } = useContext(GameData);
  const navigate = useNavigate();

  async function addGame(e) {
    e.preventDefault()
    await axios.post("http://192.168.50.99:3307/api", {
      title: title,
      BrotherScore: BrotherScore,
      BigManScore: BigManScore,
      consoleUsed: consoleUsed,
      BrotherStoredComment: BrotherStoredComment,
      BigManStoredComment: BigManStoredComment,
      info: info,
      moby_id: moby_id,
      platforms: platforms,
      release_date: release_date,
      developers: developers,
      publishers: publishers,
      moby_score: moby_score,
      genre: genre,
      perspective: perspective,
      gameplay: gameplay,
      interfacedata: interfacedata,
      narrative: narrative,
      misc: misc,
      description: description,
      moby_url: moby_url,
      screenshots_urls: screenshots_urls
    })
      .then(response => response)
      .then(response => {
        console.log(response);
      })
    setStoredGameList(null);
  }

  function button() {
    navigate("/")
  }

  return (
    <div>
      <h1>Add New Game</h1>
      <p>NOTE: Fields can be empty on submit</p>
      <button onClick={button}> back to main </button>
      <div className="addgameForm">
        <form onSubmit={addGame}>
          <label htmlFor="title">Title:</label>
          <input type="text" id="title" value={title} onChange={e => setTitle(e.target.value)} />
          <label htmlFor="brotherscore">Brother Score:</label>
          <input type="number" id="brotherscore" value={BrotherScore} onChange={e => setBrotherScore(e.target.value)} />
          <label htmlFor="bigmanscore">BigMan Score:</label>
          <input type="number" id="bigmanscore" value={BigManScore} onChange={e => setBigManScore(e.target.value)} />
          <label htmlFor="consoleused">Console Used:</label>
          <input type="text" id="consoleused" value={consoleUsed} onChange={e => setConsoleUsed(e.target.value)} />
          <label htmlFor="brothercomment">Brother Comment:</label>
          <input type="text" id="brothercomment" value={BrotherStoredComment} onChange={e => setBrotherComment(e.target.value)} />
          <label htmlFor="bigmancomment">BigMan Comment:</label>
          <input type="text" id="bigmancomment" value={BigManStoredComment} onChange={e => setBigManComment(e.target.value)} />
          <label htmlFor="info">Info:</label>
          <input type="text" id="info" value={info} onChange={e => setInfo(e.target.value)} />
          <label htmlFor="moby_id">Moby ID:</label>
          <input type="number" id="moby_id" value={moby_id} onChange={e => setMoby_id(e.target.value)} />
          <label htmlFor="platform">Platform:</label>
          <input type="text" id="platform" value={platforms} onChange={e => setPlatform(e.target.value)} />
          <label htmlFor="release_date">Release Date:</label>
          <input type="date" id="release_date" value={release_date} onChange={e => setRelease_date(e.target.value)} />
          <label htmlFor="developers">Developers:</label>
          <input type="text" id="developers" value={developers} onChange={e => setDevelopers(e.target.value)} />
          <label htmlFor="publishers">Publishers:</label>
          <input type="text" id="publishers" value={publishers} onChange={e => setPublishers(e.target.value)} />
          <label htmlFor="moby_score">Moby Score:</label>
          <input type="number" id="moby_score" value={moby_score} onChange={e => setMoby_score(e.target.value)} />
          <label htmlFor="genre">Genre:</label>
          <input type="text" id="genre" value={genre} onChange={e => setGenre(e.target.value)} />
          <label htmlFor="perspective">Perspective:</label>
          <input type="text" id="perspective" value={perspective} onChange={e => setPerspective(e.target.value)} />
          <label htmlFor="gameplay">Gameplay:</label>
          <input type="text" id="gameplay" value={gameplay} onChange={e => setGameplay(e.target.value)} />
          <label htmlFor="interface">Interface:</label>
          <input type="text" id="interface" value={interfacedata} onChange={e => setInterface(e.target.value)} />
          <label htmlFor="narrative">Narrative:</label>
          <input type="text" id="narrative" value={narrative} onChange={e => setNarrative(e.target.value)} />
          <label htmlFor="misc">Misc:</label>
          <input type="text" id="misc" value={misc} onChange={e => setMisc(e.target.value)} />
          <label htmlFor="description">Description:</label>
          <input type="text" id="description" value={description} onChange={e => setDescription(e.target.value)} />
          <label htmlFor="moby_url">Moby URL:</label>
          <input type="text" id="moby_url" value={moby_url} onChange={e => setMoby_url(e.target.value)} />
          <label htmlFor="screenshots_urls">Screenshots URLs:</label>
          <input type="text" id="screenshots_urls" value={screenshots_urls} onChange={e => setScreenshots_urls(e.target.value)} />
          <button type="submit">Add Game</button>
        </form>
      </div>
    </div>
  );
}

export default AddNewGame;