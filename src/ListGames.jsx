import { useContext, useEffect, useState, useRef } from 'react'
import axios from "axios"
import { useNavigate } from 'react-router-dom';
import { GameData } from './DataStore';
import Charts from './charts';


const ListGames = () => {
  const navigate = useNavigate();

  //UseContext to move data from this page to other pages
  const { setselectedGame, setStoredGameList } = useContext(GameData);

  const [gamelist, setgamelist] = useState(null);
  const [FromWhereToDisplay, setFromWhereToDisplay] = useState(0);
  const [HowManyGamesToDisplay, setHowManyGamesToDisplay] = useState(25);
  const [HowManyGamesToDisplayStore, setHowManyGamesToDisplayStore] = useState(25); //Used to change the amount of games to display without changing the original user defined amount
  const [searchGameByName, SetSearchGameByName] = useState("");

  const gameIndex = useRef(0); //Used to keep track of the how many games are being displayed and used to prevent the user from going forward if no items are left to display

  //fetch data from the API
  useEffect(() => {
    axios.get("http://192.168.50.99:3307/api")
      .then(response => {
        setgamelist(response.data);
        setStoredGameList(response.data);
      })
  }, [setStoredGameList])

  //Take user to main page
  function button() {
    navigate("/");
  }

  //Change the list back
  function buttonBack() {
    if (parseInt(FromWhereToDisplay) <= 0) {
      setFromWhereToDisplay(0);
    } if ((parseInt(FromWhereToDisplay) - parseInt(HowManyGamesToDisplay)) <= 0) {
      setFromWhereToDisplay(0);
      setHowManyGamesToDisplayStore(parseInt(HowManyGamesToDisplay));
    } else {
      setFromWhereToDisplay(parseInt(FromWhereToDisplay) - parseInt(HowManyGamesToDisplay));
      setHowManyGamesToDisplayStore(parseInt(HowManyGamesToDisplayStore) - parseInt(HowManyGamesToDisplay));
    }
  }

  //change the list forward
  function buttonForward() {
    if ((gameIndex.current +1) < HowManyGamesToDisplay) {
      return;
    }
    setFromWhereToDisplay(parseInt(FromWhereToDisplay) + parseInt(HowManyGamesToDisplay));
    setHowManyGamesToDisplayStore(parseInt(HowManyGamesToDisplayStore) + parseInt(HowManyGamesToDisplay));
  }

  //Take a the title of a game and store it in the context to hand it over to other pages then move user to that games page
  function StoreGameName(event) {
    setselectedGame(event.target.textContent)
    navigate("./game");
  }

  //used to display Loading... when the data is not yet ready to prevent "Cannot read property 'map' of null"
  function loading() {
    return (
      <>
        <p>Loading...</p>
      </>
    )
  }

  //Change the amount of games to display
  function amountOfGames(event) {
    //prevent negative numbers
    if (event.target.value < 0) {
      setHowManyGamesToDisplay(0);
      setHowManyGamesToDisplayStore(0);
    } else {
      setHowManyGamesToDisplay(event.target.value);
      setHowManyGamesToDisplayStore(event.target.value);
    }
  }

  //Search game by its title
  function searchByName(event) {
    SetSearchGameByName(event.target.value);
  }

  //The main content of the page
  function renderMainPage() {

    return (
      <>
        <div className='Charts'>
          <Charts></Charts>
        </div>
        <div className='gamelist'>
          <h3>Game List</h3>
          <div className='gamelistTop'>
            <p>Title to search: </p>
            <input id="namesearch" defaultValue="" onChange={searchByName}></input>
            <button onClick={button}> back to main </button>
            <p>How many to display: </p>
            <input id="numbersearch" defaultValue="25" onChange={amountOfGames} type="number" min="0"></input>
          </div>
          <div className='gamelistGames'>
            <div className='gamelistSingleGameTitle'>
              <p className='gametitle'>Title</p>
              <p>Release Date</p>
              <p>Genre</p>
              <p>Publisher</p>
              <p>Developer</p>
              <p>consoles</p>
            </div>
            {gamelist
              .filter(game => game.title.includes(searchGameByName.charAt(0).toUpperCase() + searchGameByName.slice(1))) //Make sure the first letter is always uppercase and add the rest to the string
              .slice(FromWhereToDisplay, HowManyGamesToDisplayStore)
              .map((game, index) => {
                gameIndex.current = index;
                return (
                  <div key={index} className='gamelistSingleGame'>
                    <p onClick={StoreGameName}>{game.title}</p>
                    <p>{game.release_date}</p>
                    <p>{game.genre}</p>
                    <p>{game.publishers}</p>
                    <p>{game.developers}</p>
                    <p>{game.platforms}</p>
                  </div>
                )
              })}
          </div>
          <div className='gamelistBottomNavigation'>
            <button onClick={buttonBack}>Back</button>
            <button onClick={buttonForward}>Forward</button>
          </div>
        </div>
      </>
    )
  }

  //If data is not yet ready display Loading...
  if (gamelist === null) {
    return loading();
  } else {
    return renderMainPage();
  }
}

export default ListGames;