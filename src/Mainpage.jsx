import { useNavigate } from "react-router-dom"

const Mainpage = () => {
  const navigate = useNavigate();

  function button() {
    navigate("/gamelist")
  }

  function addgame() {
    navigate("/addnewgame")
  }

  return (
    <>
      <h1>Main Page</h1>
      <button onClick={button}> List games </button>
      <button onClick={addgame}> Add New Game </button>
    </>
  )
}

export default Mainpage;