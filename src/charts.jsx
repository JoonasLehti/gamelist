import { useContext, useEffect, useState } from 'react';
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie, Bar } from 'react-chartjs-2';
import { GameData } from './DataStore';

ChartJS.register(ArcElement, CategoryScale, LinearScale, BarElement, Tooltip, Legend);
const Charts = () => {

  const { storedGameList } = useContext(GameData);

  const [averageBrotherRating, setAverageBrotherRating] = useState(0);
  const [averageBigManRating, setAverageBigManRating] = useState(0);
  const [genreCounts, setGenreCounts] = useState({});

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Chart.js Bar Chart',
      },
    },
  };

  const LineData = {
    labels: ['Average scores'],
    datasets: [
      {
        label: 'Brother',
        data: [averageBrotherRating, 10],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
        ],
        borderWidth: 1,
      },
      {
        label: 'Bigman',
        data: [averageBigManRating, 10],
        backgroundColor: [
          'rgba(54, 162, 235, 0.2)',
        ],
        borderColor: [
          'rgba(54, 162, 235, 1)',
        ],
        borderWidth: 1,
      },
    ],
  };

  const PieData = {
    labels: ['Action', 'Racing', 'Driving', 'Strategy', 'Adventure', 'Simulation', 'tactics', 'Role-playing (RPG)', 'Sports', 'Compilation', "No Genre", 'Add-on', 'Puzzle'],
    datasets: [
      {
        label: 'Average',
        data: [
          genreCounts["Action"],
          genreCounts["Racing"],
          genreCounts["Driving"],
          genreCounts["Strategy"],
          genreCounts["Adventure"],
          genreCounts["Simulation"],
          genreCounts["tactics"],
          genreCounts["Role-playing (RPG)"],
          genreCounts["Sports"],
          genreCounts["Compilation"],
          genreCounts[""],
          genreCounts["Add-on"],
          genreCounts["Puzzle"],
        ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(1, 255, 132, 0.2)',
          'rgba(1, 1, 255, 0.2)',
          'rgba(1, 255, 255, 0.2)',
          'rgba(255, 1, 255, 0.2)',
          'rgba(255, 255, 1, 0.2)',
          'rgba(128, 0, 0, 0.2)',
          'rgba(0, 128, 0, 0.2)',
          'rgba(0, 0, 128, 0.2)',
          'rgba(128, 128, 0, 0.2)',
          'rgba(128, 0, 128, 0.2)',
          'rgba(0, 128, 128, 0.2)',
          'rgba(192, 192, 192, 0.2)',
          'rgba(128, 128, 128, 0.2)'

        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(1, 255, 132, 1)',
          'rgba(1, 1, 255, 1)',
          'rgba(1, 255, 255, 1)',
          'rgba(255, 1, 255, 1)',
          'rgba(255, 255, 1, 1)',
          'rgba(128, 0, 0, 1)',
          'rgba(0, 128, 0, 1)',
          'rgba(0, 0, 128, 1)',
          'rgba(128, 128, 0, 1)',
          'rgba(128, 0, 128, 1)',
          'rgba(0, 128, 128, 1)',
          'rgba(192, 192, 192, 1)',
          'rgba(128, 128, 128, 1)'

        ],
        borderWidth: 1,
      },
    ],
  };

  useEffect(() => {
    let sum = 0;
    let count = 0;
    let genreCounts = {};

    storedGameList.forEach((game) => {
      const rating = parseFloat(game["brother-rating"]);
      if (rating === 0) {
        return;
      }
      if (!isNaN(rating)) {
        sum += rating;
        count++;
      }

      // Count genres
      const genres = game["genre"].split(/[,/]/);
      genres.forEach((genre) => {
        const trimmedGenre = genre.trim();
        if (trimmedGenre !== "" && genreCounts[trimmedGenre]) {
          genreCounts[trimmedGenre]++;
        } else if (trimmedGenre !== "") {
          genreCounts[trimmedGenre] = 1;
        }
      });
    });
    setAverageBrotherRating(sum / count);

    sum = 0;
    count = 0;

    storedGameList.forEach((game) => {
      const rating = parseFloat(game["bigman-rating"]);
      if (rating === 0) {
        return;
      }
      if (!isNaN(rating)) {
        sum += rating;
        count++;
      }
    });
    setAverageBigManRating(sum / count);
    setGenreCounts(genreCounts);
  }, [storedGameList]);


  return (
    <div>
      <Bar options={options} data={LineData} />
      <Pie data={PieData} />
    </div>
  )
}

export default Charts;