import express from 'express';
import mariadb from 'mariadb';
import cors from 'cors';
import axios from 'axios';

/*const express = require('express');
const mariadb = require('mariadb');*/
const app = express();
const port = 3307;

const pool = mariadb.createPool({
  host: '192.168.50.85',
  user: 'react',
  password: 'react1234',
  database: 'CSV_DB 9',
  port: 3307,
  connectionLimit: 5,
  supportBigNumbers: true,
  bigNumberStrings: true
});

app.use(cors());
app.use(express.json());

//Prints log on console window at startup
app.listen(port, '0.0.0.0',() => {
  console.log(`Server is running on http://localhost:${port}`);
});

app.get('/api', async (req, res) => {
  let conn;
  try {
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM `Multiplayer Games V2` ORDER BY title ASC');
    res.json(rows);
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  } finally {
    if (conn) conn.end();
  }
});

app.put('/api', async (req, res) => {
  const { selectedGame, BrotherStoredComment, BigManStoredComment, BrotherRating, BigManRating } = req.body;
  let conn;
  try {
    conn = await pool.getConnection();
    const query = 'UPDATE `Multiplayer Games V2` SET `brother-rating` = ?, `bigman-rating` = ?, `brother-comments` = ?, `bigman-comments` = ? WHERE `title` = ?';
    const params = [BrotherRating, BigManRating, BrotherStoredComment, BigManStoredComment, selectedGame];
    const rows = await conn.query(query, params);
    res.json(rows);
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  } finally {
    if (conn) conn.end();
  }
});

app.post('/api', async (req, res) => {
  const { title, BrotherScore, BigManScore, consoleUsed, BrotherStoredComment, BigManStoredComment, info, moby_id, platforms, release_date, developers, publishers, moby_score, genre, perspective, gameplay, interfacedata, narrative, misc, description, moby_url, screenshots_urls } = req.body;
  let conn;
  try {
    conn = await pool.getConnection();
    const query = 'INSERT INTO `Multiplayer Games V2` (`title`, `brother-rating`, `bigman-rating`, `console-used`,`brother-comments`, `bigman-comments`, `info`, `moby_id`, `platforms`, `release_date`, `developers`, `publishers`, `moby_score`, `genre`, `perspective`, `gameplay`, `interface`, `narrative`, `misc`, `description`, `moby_url`, `screenshot_urls`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    const params = [title, BrotherScore, BigManScore, consoleUsed, BrotherStoredComment, BigManStoredComment, info, moby_id, platforms, release_date, developers, publishers, moby_score, genre, perspective, gameplay, interfacedata, narrative, misc, description, moby_url, screenshots_urls];
    const rows = await conn.query(query, params);
    res.json(rows);
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  } finally {
    if (conn) conn.end();
  }
});

app.get('/mobyapi', async (req, res) => {
  const { title, api_key } = req.query;
  try {
    const response = await axios.get('https://api.mobygames.com/v1/games', {
      params: {
        title,
        api_key
      }
    });
    res.json(response.data);
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
});

app.delete('/api', async (req, res) => {
  const { selectedGame } = req.body;
  let conn;
  try {
    conn = await pool.getConnection();
    const query = 'DELETE FROM `Multiplayer Games V2` WHERE `title` = ?';
    const params = [selectedGame];
    const rows = await conn.query(query, params);
    res.json(rows);
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  } finally {
    if (conn) conn.end();
  }
});
