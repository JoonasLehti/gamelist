import { createContext, useEffect, useState } from "react";
import PropTypes from "prop-types";


export const GameData = createContext();

export const GameDataProvider = ({ children }) => {
  const [selectedGame, setselectedGame] = useState("");
  const [API_Key, setAPI_Key] = useState();
  const [storedGameList, setStoredGameList] = useState(null);

  useEffect(() => {
    fetch("./private/token.json")
    .then (response => response.json())
    .then(data => {
      setAPI_Key(data.api_key)
    })
  }, [])

  return(
    <GameData.Provider value={{selectedGame, setselectedGame, API_Key, setAPI_Key, storedGameList, setStoredGameList}}>
      {children}
    </GameData.Provider>
  );
};

GameDataProvider.propTypes = {
  children: PropTypes.element
};