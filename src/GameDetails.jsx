import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GameData } from "./DataStore";
import Modal from 'react-modal';
import axios from "axios";

const GameDetails = () => {
  const navigate = useNavigate();
  const { selectedGame, API_Key, storedGameList } = useContext(GameData);

  Modal.setAppElement('#root');

  const [fetchdata, setfetchdata] = useState(null);
  const [singleStoredGame, setSingleStoredGame] = useState("");
  const [BrotherComment, setBrotherComment] = useState("");
  const [BigManComment, setBigManComment] = useState("");
  const [BrotherRating, setBrotherRating] = useState("");
  const [BigManRating, setBigManRating] = useState("");
  const [modalIsOpen, setIsOpen] = useState(false);
  const [modalMessage, setModalMessage] = useState("");
  const [modalTitle, setModalTitle] = useState("");

  //Move back to the full list of games
  function button() {
    navigate("/gamelist");
  }

  const [locallist, setlocallist] = useState(null);

  //Fetch data from the API
  useEffect(() => {
    try {
      axios.get("http://192.168.50.99:3307/api")
        .then(response => response.data)
        .then(data => {
          const found = data.find((game) => game.title === selectedGame);
          setlocallist(found);
          setBrotherComment(found["brother-comments"]);
          setBigManComment(found["bigman-comments"]);
          setBrotherRating(found["brother-rating"]);
          setBigManRating(found["bigman-rating"]);
          setSingleStoredGame(storedGameList.find((game) => game.title === selectedGame));
        })
    } catch (error) {
      console.log(error);
    }
  }, [selectedGame, storedGameList])

  //Fetch data from mobygames API
  useEffect(() => {
    try {
      axios.get(`http://192.168.50.99:3307/mobyapi`, {
        params: {
          title: selectedGame,
          api_key: API_Key
        }
      })
        .then(response => {
          setfetchdata(response.data.games);
        })
    } catch (error) {
      console.log(error);
    }
  }, [API_Key, selectedGame]);

  //Display Loading... when the data is not yet ready to prevent "Cannot read property 'map' of null"
  function loading() {
    return (
      <>
        <p>Loading...</p>
      </>
    )
  }

  function getBrotherComment(event) {
    setBrotherComment(event.target.value);
  }

  function getBigManComment(event) {
    setBigManComment(event.target.value);
  }

  function getBrotherRating(event) {
    setBrotherRating(event.target.value);
  }

  function getBigManRating(event) {
    setBigManRating(event.target.value);
  }

  //Send data to the API updating the comments
  async function UpdateComments() {
    await axios.put("http://192.168.50.99:3307/api", {
      selectedGame: selectedGame,
      BrotherStoredComment: BrotherComment,
      BigManStoredComment: BigManComment,
      BrotherRating: BrotherRating,
      BigManRating: BigManRating
    })
      .then(response => response)
      .then(response => {
        if (response.status === 200) {
          setModalMessage("Comments were updated successfully!");
          setModalTitle("Success!");
          setIsOpen(true);
        } else {
          alert("Something went wrong, please try again later");
        }
        console.log(response);
      })
  }

  //Delete game from the database
  async function deleteGame() {
    if (confirm("!WARNING! This will delete the game PERMANENTLY from the database") == true) {
      await axios.delete("http://192.168.50.99:3307/api", {
        data: {
          selectedGame: selectedGame,
        }
      })
        .then(response => response)
        .then(response => {
          if (response.status === 200) {
            setModalMessage("Game was deleted successfully!");
            setModalTitle("Success!");
            setIsOpen(true);
          } else {
            alert("Something went wrong, please try again later");
          }
          console.log(response);
        })
    } else {
      return;
    }
  }

  function closeModal() {
    if (modalMessage === "Game was deleted successfully!") {
      setIsOpen(false);
      navigate("/gamelist");
    } else {
      setIsOpen(false);
    }
  }

  //The main content of the page
  function renderMainContent() {
    return (
      <>
        <div id="mainContentRoot" className="gamepage">
          <div className="gamedetailsTopNavigation">
            <button onClick={button}>back to list</button>
            <button onClick={UpdateComments}>Update rating/comment</button>
            <button onClick={deleteGame}>Delete Game</button>
          </div>
          <h1>{selectedGame}</h1>
          <div className="gamedetails">
            {fetchdata[0] && fetchdata[0].sample_cover ? <img src={fetchdata[0].sample_cover.thumbnail_image}></img> : <p></p>}
            <div>
              <p>Release Date: </p> <p>{singleStoredGame.release_date}</p>
              <p>Genre: </p> <p>{singleStoredGame.genre}</p>
              <p>consoles: </p> <p>{singleStoredGame.platforms}</p>
              <p>Description: </p><p>{singleStoredGame.description}</p>
              {locallist ? <><p>Brother rating: </p><textarea value={BrotherRating} onChange={getBrotherRating}></textarea></> : <><p>Brother rating: </p><p></p></>}
              {locallist ? <><p>Brother comments: </p><textarea value={BrotherComment} onChange={getBrotherComment}></textarea></> : <><p>Brother comments: </p><p></p></>}
              {locallist ? <><p>BigMan rating: </p><textarea value={BigManRating} onChange={getBigManRating}></textarea></> : <><p>Bigman rating: </p><p></p></>}
              {locallist ? <><p>BigMan comments: </p><textarea value={BigManComment} onChange={getBigManComment}></textarea></> : <><p>BigMan comments: </p><p></p></>}
            </div>
          </div>
        </div>
        <div className="imageDisplay">
          {singleStoredGame.screenshot_urls ?
            singleStoredGame.screenshot_urls
              .split(",")
              .map((screenshot, index) => {
                return <img key={index} src={screenshot}></img>
              }) : null}
        </div>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          contentLabel="Success Modal"
          style={{
            overlay: {
              backgroundColor: 'rgba(0, 0, 0, 0.5)' 
            },
            content: {
              textAlign: 'center',
              color: 'black', 
              backgroundColor: 'white', 
              padding: '20px', 
              borderRadius: '10px', 
              width: '200px', 
              height: '200px', 
              margin: 'auto', 
            }
          }}
        >
          <h2>{modalTitle}</h2>
          <p>{modalMessage}</p>
          <button onClick={closeModal}>Close</button>
        </Modal>
      </>
    )
  }

  //If data is not yet ready display Loading... | If the API returns no data display a message | If the data is ready display the main content
  if (fetchdata === null) {
    return loading();
  } else {
    return renderMainContent();
  }
}

export default GameDetails;