# Game server list

Goal of this project is to create a visual web app to read/write a database ment mainly for my own personal use. Instead of relying on 1000+ rows of Excel sheet or manually writing into mySQL database the idea is to use this react web app instead to go through the data in much better format.

The app also takes advantage of moby games API (https://www.mobygames.com/info/api/) to fetch information of each game and populate the data on runtime (when user views a specific game the data for the game is fetched through the API)

The app also uses 'NPM express' (https://www.npmjs.com/package/express) and 'NPM mariadb' (https://www.npmjs.com/package/mariadb) to fetch data from mySQL DB as react cannot directly access a database.

# Usage
"Work in progress"

1. running 'node server.js' will start a local server with the address and password specified in the same file.
2. running 'npm run dev' to run the vite + react app

## NOTE!

1. the API key is held inside a folder called 'private' at the root of the project (This file is not shared in the git for obvious reasons).
2. simply add a token.json file to 'private' at the root in following format
  {
  "api_key": "key here inside a string"
  }


### React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh
