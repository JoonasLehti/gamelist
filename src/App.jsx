import './App.css'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Mainpage from './Mainpage'
import ListGames from './ListGames'
import GameDetails from './GameDetails'
import { GameDataProvider } from './DataStore'
import AddNewGame from './AddNewGame'

function App() {
  return (
    <>
    <GameDataProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Mainpage></Mainpage>} />
          <Route path="/gamelist" element={<ListGames></ListGames>} />
          <Route path="/gamelist/game" element={<GameDetails></GameDetails>} />
          <Route path="/addnewgame" element={<AddNewGame></AddNewGame>} />
        </Routes>
      </Router>
    </GameDataProvider>
    </>
  )
}

export default App
